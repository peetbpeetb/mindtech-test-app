export default (theme) => ({
  movieList: {
    root: {
      width: "50%",
      maxWidth: 768,
      backgroundColor: theme.palette.background.paper,
    },
  },
});
