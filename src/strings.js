export default {
  noDetailsAvailable: `No details available`,
  wikipediaResult: `Wikipedia result for ":movie"`,
  readMoreOnWiki: `Read more on Wikipedia`,
  relatedResults: `Related movies`,
  movieOnImdbLink: `:movie on IMDB`,
  title: `Title`,
  category: `Category`,
  rating: `Rating`,
  noResultsFor: `No results for ":movie"`,
  searchFieldLabel: `Enter a movie title...`,
};
