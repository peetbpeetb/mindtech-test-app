import React from "react";
import "./App.css";
import SearchField from "./components/SearchField";
import MovieList from "./components/MovieList";

export default () => (
  <div className="App">
    <SearchField />
    <MovieList />
  </div>
);
