import React, { useState } from "react";
import { ListItem, Button, Grid, CircularProgress } from "@material-ui/core";
import { observer } from "mobx-react-lite";
import MovieDetail from "./MovieDetail";
import Store from "../services/store";
import { fetchWikipediaDataByTitle } from "../services/fetch";

const store = Store.get();

export default observer(({ movie, isHeader }) => {
  const [showDetails, setShowDetails] = useState(false);
  const [detailsLoading, setDetailsLoading] = useState(false);
  const [noData, setNoData] = useState(false);

  // load data if needed
  const onTitleClick = async () => {
    if (!movie.details.wikipedia && !noData) {
      setDetailsLoading(true);
      const result = await fetchWikipediaDataByTitle(movie.title);
      if (result.message) {
        setNoData(true);
      } else {
        store.setWikipediaData(movie.id, result);
      }
      setDetailsLoading(false);
    }
    setShowDetails(!showDetails);
  };

  const TitleItem = () => {
    return (
      <Grid item xs={4}>
        {isHeader ? (
          <span>{movie.title}</span>
        ) : (
          <Button onClick={onTitleClick}>{movie.title}</Button>
        )}
      </Grid>
    );
  };

  const GenreItem = () => (
    <Grid item xs={4} className="centerCell">
      {movie.details && !isHeader
        ? movie.details.genres.map((genre) => genre.name).join(", ")
        : ""}
      {isHeader ? "Genre" : null}
    </Grid>
  );

  const RatingItem = () => (
    <Grid item xs={4} className="centerCell">
      {movie.rating}
    </Grid>
  );

  const DetailBlock = () =>
    showDetails && movie.details ? (
      <MovieDetail movie={movie} noData={noData} />
    ) : null;

  const LoadingBlock = () => (detailsLoading ? <CircularProgress /> : null);

  return (
    <ListItem className="movieListItem">
      <Grid container>
        <TitleItem />
        <RatingItem />
        <GenreItem />
      </Grid>
      <LoadingBlock />
      <DetailBlock />
    </ListItem>
  );
});
