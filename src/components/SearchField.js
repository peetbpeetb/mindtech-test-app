import React from "react";
import { TextField } from "@material-ui/core";
import { observer } from "mobx-react-lite";
import Store from "../services/store";
import { search } from "../services/search";
import getString from "../services/string";

const store = Store.get();

export default observer(() => {
  const onSearchFieldChange = ({ target }) => {
    store.setSearchPhrase(target.value);
  };

  const onSearchSubmit = (event) => {
    event.preventDefault();
    search();
  };

  return (
    <form onSubmit={onSearchSubmit} className="searchFieldForm">
      <TextField
        className="searchField"
        value={store.searchPhrase}
        label={getString("searchFieldLabel")}
        onChange={onSearchFieldChange}
        disabled={store.appIsLoading}
      />
    </form>
  );
});
