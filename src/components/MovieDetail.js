import React from "react";
import { List, ListItem, Link, Button } from "@material-ui/core";
import { search, searchSimilar } from "../services/search";
import getString from "../services/string";

export default ({ movie, noData }) => {
  const NoDataBlock = () => <div>{getString("noDetailsAvailable")}</div>;

  const WikiLink = () => (
    <Link
      target="_blank"
      href={`http://en.wikipedia.org/wiki/${movie.details.wikipedia.title
        .split(" ")
        .join("_")}`}
    >
      {" "}
      {getString("readMoreOnWiki")}
    </Link>
  );

  const WikipediaListItem = () => (
    <ListItem className="listItemWiki">
      <h4>
        {getString("wikipediaResult", {
          movie: movie.title,
        })}
        :
      </h4>
      <span>
        <span
          dangerouslySetInnerHTML={{
            __html: `${movie.details.wikipedia.snippet}...`,
          }}
        ></span>
        <WikiLink movie={movie} />
      </span>
    </ListItem>
  );

  const onRelatedSearchClick = () => searchSimilar(movie.id, movie.title);

  const LinksListItem = () => (
    <ListItem className="detailLinks">
      <Link
        target="_blank"
        href={`http://www.imdb.com/title/${movie.details.imdbID}`}
      >
        {getString("movieOnImdbLink", { movie: movie.title })}
      </Link>
      <Button onClick={onRelatedSearchClick}>
        {getString("relatedResults")}
      </Button>
    </ListItem>
  );

  return noData ? (
    <NoDataBlock />
  ) : (
    <List>
      <WikipediaListItem />
      <LinksListItem />
    </List>
  );
};
