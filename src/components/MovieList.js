import React from "react";
import { List, CircularProgress, Divider } from "@material-ui/core";
import MovieListItem from "../components/MovieListItem";
import Store from "../services/store";
import { observer } from "mobx-react";
import getString from "../services/string";
import useStyles from "../services/style";

const store = Store.get();

export default () => {
  const listHeader = {
    title: getString("title"),
    category: getString("category"),
    rating: getString("rating"),
  };

  // display loading indicator when needed
  const LoadingIndicator = observer(() =>
    store.appIsLoading ? <CircularProgress /> : null
  );

  // display a "no result for $title" message if needed
  const NoResults = observer(() =>
    store.searchResult && !store.searchResult.length && !store.appIsLoading ? (
      <div>
        {getString("noResultsFor", { movie: store.currentSearchPhrase })}
      </div>
    ) : null
  );

  // displays list only if there is data to be displayed
  // and app is not loading
  const ListBlock = observer(() => {
    const classes = useStyles("movieList");
    return store.searchResult &&
      store.searchResult.length &&
      !store.appIsLoading ? (
      <List className={classes.root}>
        <MovieListItem movie={listHeader} isHeader />
        <Divider />
        {store.searchResult.map((movie) => (
          <React.Fragment key={`movie-${movie.id}`}>
            <MovieListItem movie={movie} />
            <Divider />
          </React.Fragment>
        ))}
      </List>
    ) : null;
  });

  return (
    <React.Fragment>
      <ListBlock />
      <NoResults />
      <LoadingIndicator />
    </React.Fragment>
  );
};
