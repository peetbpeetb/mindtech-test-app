import {
  queryWikipediaDataByTitle,
  queryTMDBWDataByTitle,
  queryTMDBWDataById,
} from "./query";
import { objectLength } from "../utils";

// Fetches data with queryTMDBDataByTitle
// service call and processes the result
export const fetchTMDBDatayByTitle = async (title) => {
  let result = await queryTMDBWDataByTitle(title);
  try {
    const movies = [];
    result.data.search.edges.map(
      ({ node }) => node && objectLength(node) && movies.push(node)
    );
    result = movies;
  } catch {
    result = {
      message: "No results",
    };
  }
  return result;
};

// Fetches data with queryWikipediaDataByTitle
// service call and processes the result
export const fetchWikipediaDataByTitle = async (title) => {
  let result = await queryWikipediaDataByTitle(title);
  try {
    // assume we have at least one result
    // if not, throw an error
    if (!result.query.search.length) {
      throw "error";
    }
    result = result.query.search[0];
  } catch {
    return {
      message: "No results",
    };
  }
  return result;
};

export const fetchTMDBSimilarMoviesById = async (id) => {
  let result = await queryTMDBWDataById(id);
  try {
    const movies = [];
    result.data.movies.movie.similar.edges.map(
      ({ node }) => node && objectLength(node) && movies.push(node)
    );
    result = movies;
  } catch {
    result = {
      message: "No results",
    };
  }
  return result;
};
