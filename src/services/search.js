import Store from "./store";
import { fetchTMDBDatayByTitle, fetchTMDBSimilarMoviesById } from "./fetch";

const store = Store.get();

// Search service
// Takes a title as parameter (if no title given, it uses
// the one stored). It calls fetch service, stores
// and processes result data
export const search = async (title) => {
  // prepare
  if (title) {
    store.setSearchPhrase(title);
  }
  store.setCurrentSearchPhrase();
  store.setSearchResult(null);
  store.setAppIsLoading(true);

  // fetch
  const result = await fetchTMDBDatayByTitle(store.currentSearchPhrase);

  // no results, init results with
  // an empty array stop loading
  if (result.message) {
    store.setSearchResult([]);
    store.setAppIsLoading(false);
    return;
  }

  // init results
  store.setSearchResult(result);
  store.setAppIsLoading(false);
};

// Search similar
// Takes an id and name as parameter (movie)
// It calls fetch service, stores
// and processes result data
export const searchSimilar = async (id, title) => {
  // prepare
  store.setSearchResult(null);
  store.setAppIsLoading(true);

  // fetch
  const result = await fetchTMDBSimilarMoviesById(id);

  // no results, init results with
  // an empty array stop loading
  if (result.message) {
    store.setCurrentSearchPhrase(`${title} (related)`);
    store.setSearchResult([]);
    store.setAppIsLoading(false);
    return;
  }

  // init results
  store.setSearchResult(result);
  store.setAppIsLoading(false);
};
