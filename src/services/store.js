import { makeAutoObservable } from "mobx";

// singleton
let instance;

// Store class
// The class gives one singleton instance.
// It uses mobx makeAutoObservable function
// to be observable for mobx observers
export default class Store {
  // fields
  searchPhrase = "";
  currentSearchPhrase = "";
  appIsLoading = false;
  searchResult = null;
  // access and modify functions
  setSearchPhrase(val) {
    this.searchPhrase = val;
  }
  setSearchResult(arr) {
    this.searchResult = arr;
  }
  setAppIsLoading(val) {
    this.appIsLoading = val;
  }
  setCurrentSearchPhrase(val = this.searchPhrase) {
    this.currentSearchPhrase = val;
  }
  setWikipediaData(movieID, data) {
    const movieRef = this.searchResult.filter((m) => m.id === movieID)[0];
    movieRef.details.wikipedia = data;
  }
  constructor() {
    makeAutoObservable(this);
  }
  static get() {
    if (!instance) {
      instance = new Store();
    }
    return instance;
  }
}
