import { objectLength } from "../utils";
import strings from "../strings";

// gets strings from strings module, replaces
// parameters if there is any
export default (stringKey, replaceObj = {}) => {
  try {
    if (!stringKey || !strings[stringKey]) {
      throw "error";
    }
    let string = strings[stringKey];
    if (objectLength(replaceObj)) {
      Object.keys(replaceObj).forEach((key) => {
        const val = replaceObj[key];
        string = string.replace(`:${key}`, val);
      });
    }
    return string;
  } catch {
    return `[${stringKey}]`;
  }
};
