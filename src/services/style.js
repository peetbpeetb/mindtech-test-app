import { makeStyles } from "@material-ui/core/styles";
import styles from "../styles/styles";

const getModuleStyles = (theme, module) => {
  // feed styles with theme param
  const _styles = styles(theme);
  let result;
  try {
    if (!module || !_styles[module]) {
      throw "error";
    }
    result = _styles[module];
  } catch {
    return "";
  }
  return result;
};

export default (module) =>
  makeStyles((theme) => getModuleStyles(theme, module))();
