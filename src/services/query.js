import { TMDBW_API_URL, WIKIPEDIA_API_URL } from "../config";

// Queries data from TMDBW graphql api.
// It has one parameter which is the title
// of the movie being searched.
export const queryTMDBWDataByTitle = async (title) => {
  const query = `{
      search(term: "${title}") {
        edges {
          node {
            ... on MovieResult {
              id
              title
              rating
              details {
                ... on DetailedMovie {
                  imdbID
                  genres {
                    name
                  }
                }
              }
            }
          }
        }
      }
    }`;

  const result = await fetch(TMDBW_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query,
    }),
  });
  return await result.json();
};

export const queryTMDBWDataById = async (id) => {
  const query = `
  {
    movies{
      movie(id:${id}){
        title,
        similar{
          edges{
            node {
              id
            title
            rating
            details {
              ... on DetailedMovie {
                imdbID
                genres {
                  name
                }
              }
            }
            }
          }
        }
        
      }
    }
  
}`;
  const result = await fetch(TMDBW_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query,
    }),
  });
  return await result.json();
};

// Fetches data from wikipedia REST api.
// It has one parameter which is the title
// of the movie being searched.
export const queryWikipediaDataByTitle = async (title) => {
  const result = await fetch(
    `${WIKIPEDIA_API_URL}?action=query&format=json&srsearch=${title}&list=search&srprop=snippet&srlimit=1`
  );
  return await result.json();
};
