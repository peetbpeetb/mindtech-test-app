export const TMDBW_API_URL =
  process.env.TMDBW_API_URL || "http://localhost:8010/proxy/";
export const WIKIPEDIA_API_URL =
  process.env.WIKIPEDIA_API_URL || "http://localhost:8011/proxy";
